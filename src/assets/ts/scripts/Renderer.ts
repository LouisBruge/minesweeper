/**
 * IGame defines methods and properties exposed by a Game Instance
 *
 * @interface {IGame}
 */
export interface IGame {
   readonly alive: boolean;
   handleCellClick(td: any, i: number, y: number): void;
}

/**
 * Renderer is a helper class in charge to HTML rendering
 *
 * @class {Renderer}
 */
export class Renderer {
  /**
   * generateCellID generates cell id given this coords
   *
   * @param {number} x - coord on the x-axis
   * @param {number} y - coord on the y-axis
   *
   * @returns {string}
   */
  static generateCellID(x: number, y: number): string {
    return `cell-${x}-${y}`;
  }

  /**
   * generateTable generates HTML table from number of cols and rows provided
   *
   * @param {number} rows - number of rows
   * @param {number} cols - number of cols
   * @param {Game} game - game instance
   *
   * @returns {HTMLTableElement}
   */
  static generateTable(rows: number, cols: number, game: IGame): HTMLTableElement {
    const table = document.createElement("table");

    for (let i = 0; i < rows; i++) {
      const row = document.createElement("tr");

      for (let j = 0; j < cols; j++) {
        const td = document.createElement("td") as any;
        td.id = Renderer.generateCellID(i, j);
        row.appendChild(td);

        td.addEventListener('mouseup', (event: MouseEvent) => {
          if (!game.alive) {
            return;
          }

          if (event.button === 2) {
            if (td.clicked) {
              return;
            }

            if (td.flagged) {
              td.flagged = false;
              td.textContent = "";
            } else {
              td.flagged = true;
              td.textContent= "F";
            }

            event.preventDefault();
            event.stopPropagation();
            return false;
          }

          game.handleCellClick(td, i, j);
       })

        td.oncontextmenu = () => false;
      }
      table.appendChild(row);
      console.log(row);
    }

    return table;
  }
}


