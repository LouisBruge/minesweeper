import {Game} from "./Game";

/**
 * ROWS defines the number of rows on the field
 *
 * @type {number}
 */
const ROWS: number = 12;


/**
 * COLS defines the number of cols on the field
 *
 * @type {number}
 */
const COLS: number = 24;

/**
 * BOMBS defines the number of bombs on the field
 *
 * @type {number}
 */
const BOMBS: number = 55;
const reload = () => window.location.reload();

window.addEventListener('load', () => {
  const lostElt = document.getElementById("lost");
  if (lostElt) {
    lostElt.style.display="none";
    }
  const game = new Game(ROWS, COLS, BOMBS);

  const fieldElt = document.getElementById("field");
  if (fieldElt) {
  fieldElt.appendChild(game.renderTable());
  }
});
