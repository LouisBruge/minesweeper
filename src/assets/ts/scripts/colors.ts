/**
 * IColorsEnum represents an object what links colors with adjacent bombs
 *
 * @interface {IColorsEnum}
 */
export interface IColorsEnum {
  [k: number]: string;
}

/**
 * colors link a color with the number of bombs around a cell
 *
 * @enum {string}
 */
export const colors: IColorsEnum = Object.freeze({
  1: "blue",
  2: "green",
  3: "red",
  4: "purple",
  5: "maroon",
  6: "turquoise",
  7: "black",
  8: "grey"
});
