import {Renderer} from "./Renderer";
import {colors} from  "./colors";

/**
 * Game represents a game instance
 *
 * @class {Game}
 */
export class Game {
  /**
   * _rows define the number of rows for the current game
   *
   * @type {number}
   */
  private _rows: number;

  /**
   * _cols defines the number of cols for the current game
   *
   * @type {number}
   */
  private _cols: number;

  /**
   * _bombs defines the number of bombs present on the field for the current game
   *
   * @type {number}
   */
  private _bombs: number;

  /**
   * _bombsCoords store bombs coordinates
   *
   * @type {Array}
   */
  private _bombsCoords: Array<boolean[]> = [];

  /**
   * _alive is a helper flag relatd to the state of the game
   *
   * @type {boolean}
   */
  private _alive: boolean;

  /**
   * initialize a new game instance
   *
   * @constructor
   *
   * @param {number} rows - number of rows
   * @param {number} cols - number of cols
   * @param {number} bombs - number of bombs in the field
   */
  constructor(rows: number, cols: number, bombs: number) {
    this._rows = rows;
    this._cols = cols;
    this._bombs = bombs;

    this._alive = true;

    this.placeBombs();
  }

  get alive() {
    return this._alive;
  }

  /**
   * renderTable renders the table
   *
   * @returns {HTMLTableElement}
   */
  renderTable(): HTMLTableElement {
    return Renderer.generateTable(this._rows, this._cols, this);
  }

  /**
   * placeBombs place bombs for the game instance
   *
   * @returns {void}
   */
  placeBombs(): void {
    let bombs: Array<boolean[]> = [];

    for(let i = 0; i < this._bombs; i ++) {
      const {row, col} = this._generateBombCoord(bombs);
      if (!bombs[row]) {
        bombs[row] = [];
      }
      bombs[row][col] = true;
    }
    this._bombsCoords = [...bombs];
  }

  /**
   * _generateBombCoord generates bomb coord given previous bombs already place
   *
   * @param {Array} previousBombs - array of coord of previous bombs
   *
   * @return {void}
   */
  _generateBombCoord(previousBombs: Array<boolean[]>): {row: number, col: number} {
    const row = Game._randomInt(this._rows);
    const col = Game._randomInt(this._cols);

    if (!!previousBombs[row] && previousBombs[row][col]) {
      return this._generateBombCoord(previousBombs);
    }
    return {row, col};
  }

  /**
   * _randomInt generates a random int given max value
   *
   * @static
   *
   * @param {number} max - max
   *
   * @return {number}
   */
  static _randomInt(max: number): number {
    return Math.floor(Math.random() * max);
  }

  /**
   * adjacentBombs returns the number of adjacent bombs given a coordonate
   *
   * @param {number} row - row
   * @param {number} col - col
   *
   * @return {number}
   */
  adjacentBombs(row: number, col: number): number {
  let nbBombs = 0;

    for (let i = -1; i < 2; i++) {
      for (let j = -1; j<2; j++) {
        if (this._bombsCoords[row + i] && this._bombsCoords[row + i][col + j]) {
          nbBombs++;
        }
      }
    }

    return nbBombs;
  }

  /**
   * adjacentFlags returns the number of adjactent flag for a given cell position
   *
   * @param {number} row - row
   * @param {number} col - col
   *
   * @return {number}
   */
  adjacentFlags(row: number, col: number): number {
    let nbFlags = 0;

    for (let i = -1; i < 2; i++) {
      for (let j = -1; j < 2; j++) {
        const cell = document.getElementById(Renderer.generateCellID(row + i, col + j)) as any;
        if (!!cell && cell.flagged) {
          nbFlags++;
        }
      }
    }
    return nbFlags;
  }

  /**
   * handleCellClick handles cell click
   *
   * @param cell - minesweeper's cell
   * @param {number} row - row
   * @param {number} col - col
   *
   */
  handleCellClick(cell: any, row: number, col: number) {
    if (!this._alive) {
      return;
    }

    if (cell.flagged) {
      return;
    }

    cell.clicked = true;

    if (this._bombsCoords[row] && this._bombsCoords[row][col]) {
      cell.style.color = "red";
      cell.textContent = "B";
      this.gameOver();
      return;
    }

    cell.style.backgroundColor = "lightGrey";
    const bombs = this.adjacentBombs(row, col);

    if (bombs) {
      cell.style.color = colors[bombs];
      cell.textContent = bombs;
      return;
    }
    this.propagateClick(row, col);
  }

  /**
   * propagateClick propagates click handle on each adjacent cells given a start position
   *
   * @param {number} row
   * @param {number} col
   */
  propagateClick(row: number, col: number) {
    for (let i= -1; i < 2; i++) {
      for (let j = -1; j < 2; j++) {
        if (i === 0 && j === 0) {
          continue;
        }

        const cell = document.getElementById(Renderer.generateCellID(row + i, col + j)) as any;
        if (!!cell && !cell.clicked && !cell.flagged) {
          this.handleCellClick(cell, row + i, col + j);
        }
      }
    }
  }
  /**
   * gameOver sets the game as lost and display button to start a new game
   */
  gameOver() {
    this._alive = false;
    const lostElt = document.getElementById("lost");
    if (lostElt) {
      lostElt.style.display="block";
    }
  }
}


