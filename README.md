# Minesweeper 

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)  
minesweeper game write on Typescript, HTML and CSS

This project based on [opengenus tutorial to create a minesweeper game using js](https://iq.opengenus.org/minesweeper-game-using-js/) and write
on HTML, CSS and Typescript. This project differs from the original tutorial due to the use of ES6 syntax and Typescript.

## Install
### Dependencies
This project require [npm](https://www.npmjs.com/) v6.14.11 and [node](https://nodejs.org/en/) v14.16.0 as dependencies. Others dependencies can be install with `make deps`
command.

[Make](https://www.gnu.org/software/make/manual/make.html) is a optional dependency. All commands can be executed with
npm scripts.

### Instructions
- Clone the repository on your computer

```sh
git clone https://gitlab.com/LouisBruge/minesweeper.git
```

- Install dependencies

```sh
make deps
```

## Usage
- Install the project on your computer (see [install section](#install))
- Build the project

```sh
make dev
```

- Open [http://localhost:1234/](http://localhost:1234) in your browser
- click left on a cell reveal its content 
- click right on a cell place a flag
- a second right click on a flagged cell remove the flag
- button `start again` reload the browser to start a new game

## Contributing
This project is currently not open to contribution.

## License
This project is unlicensed
> See more details in [LICENSE](LICENSE)
