.DEFAULT_GOAL: help

.PHONY: dev
dev: deps ## run http server on dev mode
	npm run dev

.PHONY: dev-watch
dev-watch: deps ## run http serv on dev watch mode
	npm run dev:watch

.PHONY: build
build: deps ## build source
	npm run build

.PHONY: lint
lint: deps ## run linters
	npm run lint

.PHONY: deps
deps: ## install dependencies
	npm install

.PHONY: help
help: ## display current help
	@grep -E '^[a-zA-Z0-9_\-\\$\(\)]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
